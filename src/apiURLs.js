const DebugConfig = {
  getUserInfo: 'https://asksel.ddns.net/private/getUserInfo',
  getNotes: 'https://asksel.ddns.net/private/getNotes',
  deleteNote: 'https://asksel.ddns.net/private/deleteNote',
  addNote: 'https://asksel.ddns.net/private/addNote',
  editNote: 'https://asksel.ddns.net/private/editNote',
  redirectVK: 'https://asksel.ddns.net/redirectVK',
  logout: 'https://asksel.ddns.net/private/logout'
}

const ProdConfig = {
  getUserInfo: 'https://api.simple-notes.ml/private/getUserInfo',
  getNotes: 'https://api.simple-notes.ml/private/getNotes',
  deleteNote: 'https://api.simple-notes.ml/private/deleteNote',
  addNote: 'https://api.simple-notes.ml/private/addNote',
  editNote: 'https://api.simple-notes.ml/private/editNote',
  redirectVK: 'https://api.simple-notes.ml/redirectVK',
  logout: 'https://api.simple-notes.ml/private/logout'
}


export default ProdConfig;
