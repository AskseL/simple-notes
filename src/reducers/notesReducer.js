export default (state = {}, action) => {

  const arrSearch = (arr, id) => {
    let index = -1;
    for(let i = 0; i < arr.length; i++){
      if(arr[i].id === id){
        index = i;
        break;
      }
    }
    return index;
  }

  if(action.type === 'NOTES_INIT') {
    return{
      ...state,
      notes:{
        status: 'init'
      }
    }
  }

  if(action.type === 'NOTES_GET_START'){
    return{
      ...state,
      notes:{
        status: 'loading'
      }
    }
  }

  if(action.type === 'NOTES_GET_FAILED'){
    return{
      ...state,
      notes:{
        status: 'failed'
      },
      user:{
        status: 'failed'
      }
    }
  }

  if(action.type === 'NOTES_GET_SUCCESS'){
    const payload = action.payload.map(item => {
      item.editable = false;
      return item;
    });
    return{
      ...state,
      notes:{
        status: 'success',
        payload: payload
      }
    }
  }

  if(action.type === 'NOTES_ADD_ONE'){
    return{
      ...state,
      notes:{
        status: state.notes.status,
        payload: [
          ...state.notes.payload,
          action.payload
        ]
      }
    }
  }

  if(action.type === 'NOTES_DELETE_ONE'){
    const newList = state.notes.payload.filter(item => item.id !== action.payload);
    return{
      ...state,
      notes:{
        status: state.notes.status,
        payload: newList
      }
    }
  }

  if(action.type === 'NOTE_EDIT_ONE'){
    if(action.payload === '-10'){
      return{
        ...state,
        notes:{
          ...state.notes,
          payload: state.notes.payload.filter(item => item.id !== '-10')
        }
      }
    }

    const notes = state.notes.payload;
    const editableCount = notes.filter(item => item.editable).length;
    const index = arrSearch(notes, action.payload);
    if(index !== -1 && (editableCount === 0 || notes[index].editable)) notes[index].editable = !notes[index].editable;
    return {...state};
  }

  if(action.type === 'NOTE_SAVE_ONE'){
    const notes = state.notes.payload;
    let index = arrSearch(notes, action.payload.id);
    notes[index].header = action.payload.header;
    notes[index].body = action.payload.body;
    notes[index].editable = false;
    return{...state}
  }

  if(action.type === 'NOTE_ADD_ONE_LOCALLY'){
    const notes = state.notes.payload;
    const editableCount = notes.filter(item => item.editable).length;
    if(editableCount === 0) return {
      ...state,
      notes:{
        ...state.notes,
        payload:[
          {
            id: '-10',
            header: '',
            body: '',
            editable: true
          },
          ...state.notes.payload
        ]
      }
    }; else return state;
  }

  if(action.type === 'NOTE_ADD_ONE_SUCCESS'){
    const newPayload = [
      action.payload,
      ...state.notes.payload.filter(item => item.id != '-10')
    ];
    return{
      ...state,
      notes:{
        ...state.notes,
        payload: newPayload
      }
    }
  }

  if(state.notes) return state;
  else return {
    ...state,
    notes: {
      status: 'init'
    }
  }
}
