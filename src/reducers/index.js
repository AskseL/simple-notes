import { combineReducers } from 'redux';
import user from './userReducer';
import notes from './notesReducer';

export default combineReducers({
  user,
  notes
})
