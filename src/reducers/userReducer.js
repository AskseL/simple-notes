export default (state={}, action) => {
  if(action.type === 'GET_USER_INFO_START'){
    return {
      ...state,
      user:{
        status: 'loading'
      }
    }
  }

  if(action.type === 'GET_USER_INFO_SUCCESS'){
    return {
      ...state,
      user:{
        status: 'success',
        uid: action.payload.uid,
        first_name: action.payload.first_name,
        last_name: action.payload.last_name,
        photo_100: action.payload.photo_100
      }
    }
  }

  if(action.type === 'GET_USER_INFO_FAILED'){
    return {
      ...state,
      user:{
        status: 'failed'
      }
    }
  }

  if(state.user) return state;
  else return {
    ...state,
    user:{
      status: 'init'
    }
  }
}
