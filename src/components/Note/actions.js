import axios from 'axios';

const deleteNote = (dispatch, id) => {
  axios({
    method: 'POST',
    url: global.urls.deleteNote,
    withCredentials: true,
    headers:{
      'Content-Type': 'text/plain'
    },
    data:{
      id: parseInt(id),
    }
  }).then(res => {
    if(res.status === 200){
      dispatch({
        type: 'NOTES_DELETE_ONE', payload: id
      })
    }
  })
  .catch(res => {
    dispatch({type: 'NOTES_GET_FAILED'});
    dispatch({type: 'GET_USER_INFO_FAILED'});
  })
}

const editNote = (dispatch, id) => {
  dispatch({type: 'NOTE_EDIT_ONE', payload: id});
}

const saveNote = (dispatch, note) => {
  if(note.id === '-10') addNewNote(dispatch, note)
  else updateNote(dispatch, note)
}

const addNewNote = (dispatch, note) => {
  axios({
    method: 'POST',
    url: global.urls.addNote,
    withCredentials: true,
    headers:{
      'Content-Type': 'text/plain'
    },
    data:{
      header: note.header,
      body: note.body
    }
  }).then(res => {
    if(res.status === 200) dispatch({type: 'NOTE_ADD_ONE_SUCCESS', payload: res.data.payload});
  })
  .catch(res => {
    dispatch({type: 'NOTES_GET_FAILED'});
    dispatch({type: 'GET_USER_INFO_FAILED'});
  })
}

const updateNote = (dispatch, note) => {
  axios({
    method: 'POST',
    url: global.urls.editNote,
    withCredentials: true,
    headers:{
      'Content-Type': 'text/plain'
    },
    data:{
      id: note.id,
      header: note.header,
      body: note.body
    }
  }).then(res => {
    if(res.status === 200) dispatch({type: 'NOTE_SAVE_ONE', payload: note});
  })
  .catch(res => {
    dispatch({type: 'NOTES_GET_FAILED'});
    dispatch({type: 'GET_USER_INFO_FAILED'});
  })
}

export default {
  deleteNote,
  editNote,
  saveNote
}
