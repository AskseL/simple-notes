import React from 'react';
import './style.css';
import actions from './actions';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';

import { Button, Col, FormControl, FormGroup } from 'react-bootstrap';

const Note = ({note, DeleteNote, EditNote, SaveNote}) => {

  let newHeader = {};
  let newBody = {};

  const saveNote = () => {
    SaveNote({
      ...note,
      header: newHeader.value,
      body: newBody.value
    })
  }

  const editable = (
    <Col className="note" sm={6} md={4} lg={3}>
      <div className="noteWrapper text-center googleFont">
        <FormControl className="transparent noteHeaderEdit" type="text" inputRef={ref => {
          if(ref){
            newHeader = ref;
            ref.value = note.header;
          }
        }} />
        <FormControl className="transparent noteBodyEdit" componentClass="textarea" inputRef={ref => {
          if(ref){
            newBody = ref;
            ref.value = note.body;
          }
        }} />
      </div>
      <Button onClick={() => {EditNote(note.id)}} bsStyle="default">Отменить</Button>
      <Button onClick={() => {saveNote()}} bsStyle="default">Сохранить</Button>
    </Col>
  )

  const notEditable = (
    <Col className="note" sm={6} md={4} lg={3}>
      <div className="noteWrapper text-center googleFont">
        <h1 className="noteHeader">{note.header}</h1>
        <p className="noteBody">{note.body}</p>
      </div>
      <Button onClick={() => {EditNote(note.id)}} bsStyle="default">Изменить</Button>
      <Button onClick={() => {DeleteNote(note.id)}} bsStyle="default">Удалить</Button>
    </Col>
  )

  return note.editable ? editable : notEditable;
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => ({
  DeleteNote: id => actions.deleteNote(dispatch, id),
  EditNote: id => actions.editNote(dispatch, id),
  SaveNote: newNote => actions.saveNote(dispatch, newNote)
})


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Note));
