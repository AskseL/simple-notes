import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import './style.css';
import actions from './actions';

const Header = ({user, LogOut}) => {

  const userbar = user.uid ? (
    <Nav>
      <NavItem>Добро пожаловать, {user.first_name}</NavItem>
      <NavItem onClick={LogOut}>Выйти</NavItem>
    </Nav>
  ) : (
    <NavItem href={global.urls.redirectVK}>Войти</NavItem>
  );

  const userMenu = user.uid ? (
    <Nav>
      <LinkContainer to="/notes">
  			<NavItem>Мои заметки</NavItem>
      </LinkContainer>
		</Nav>
  ) : '';

  const navbar = (
  	<Navbar inverse collapseOnSelect>
  		<Navbar.Header>
        <Navbar.Brand>
          <LinkContainer to="/">
        		<a>Simple Notes</a>
          </LinkContainer>
        </Navbar.Brand>
    		<Navbar.Toggle />
  		</Navbar.Header>
  		<Navbar.Collapse>
        {userMenu}
  			<Nav pullRight>
  				{userbar}
  			</Nav>
  		</Navbar.Collapse>
  	</Navbar>
  );

  return navbar
}

const mapStateToProps = state => state.user
const mapDispatchToProps = dispatch => ({
  LogOut: actions.logout(dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
