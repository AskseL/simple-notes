import axios from 'axios';

const logout = dispatch => () => {
  axios.get(global.urls.logout, {withCredentials: true})
  .then(res => {
    dispatch({type: 'NOTES_GET_FAILED'});
    dispatch({type: 'GET_USER_INFO_FAILED'});
  })
}

export default {
  logout
}
