import React from 'react';
import Note from '../Note';

const NotesList = ({notes}) => {
  return (
    notes.map((item, i) => (
      <Note key={i} note={item} />
    ))
  )
}


export default NotesList;
