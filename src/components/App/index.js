import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from '../Header';
import Landing from '../Landing';
import Notes from '../Notes';
import './style.css';
import actions from './actions';
import { withRouter } from 'react-router';

const App = ({user, LoadData}) => {

  if(user.status === 'init') LoadData();

  const landing = () => (<Landing user={user} />);

  return(
    <div>
      <Header user={user}/>
      <Switch>
        <Route exact path="/" render={landing} />
        <Route exact path="/notes" component={Notes} />
      </Switch>
    </div>
  )
}

const mapStateToProps = state => {
  return state.user
}

const mapDispatchToProps = dispatch => ({
  LoadData: actions.loadData(dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
