import axios from 'axios';

const loadData = dispatch => () => {
  dispatch({type: 'GET_USER_INFO_START'});

  axios.get(global.urls.getUserInfo, {withCredentials: true})
  .then(({data: user}) => {
    dispatch({type: 'GET_USER_INFO_SUCCESS', payload: user});
  })
  .catch(dispatch({type:'GET_USER_INFO_FAILED'}));
}

export default {
  loadData
}
