import React from 'react';
import { Grid, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import LandingImageBlock from '../LandingImageBlock';
import './style.css';

const Landing = ({user}) => {

  const block1 = {
    header: "Быстрые заметки",
    body: "Добавляйте свои заметки, получайте к ним доступ из любой точки мира, редактируйте, делитесь с друзьями.",
    img: "/img/paper-notes.png"
  }
  const block2 = {
    header: "Без регистрации",
    body: "Нет необходимости создавать аккаунт. Подключите свои заметки через социальные сети без регистрации и смс :)",
    img: "/img/vk_logo.png"
  }

  const userBlock = !user.uid ? (
    <div className="text-center">
      <Button href={global.urls.redirectVK} bsStyle="primary" bsSize="large">
        <span className="fa fa-lg fa-vk"></span>
         | Войти через ВКонтакте
      </Button>
    </div>
  ) : (
    <div className="googleFont text-center">
      <h1>Добро пожаловать, {user.first_name}</h1>
      <p>Теперь вы можете перейти к <Link to="/notes">своим заметкам</Link></p>
    </div>
  )

  const data = (
    <div>
      <div className="landingUserBlock">
        {userBlock}
      </div>
      <Grid>
        <Row>
          <LandingImageBlock data={block1}/>
          <LandingImageBlock data={block2}/>
        </Row>
      </Grid>
    </div>
  );

  return data;
}

export default Landing;
