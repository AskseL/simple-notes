import React from 'react';
import { Col } from 'react-bootstrap';
import './style.css';

const LandingImageBlock = ({data}) => {

  const render = (
    <Col md={6} sm={6} lg={6}>
      <div className="landingImageBlockWrapper text-center">
        <h1 className="landingImageBlockHeader">{data.header}</h1>
        <p className="landingImageBlockBody">{data.body}</p>
        <img className="landingImageBlockImage" alt="" src={data.img}></img>
      </div>
    </Col>
  )

  return render;
}

export default LandingImageBlock;
