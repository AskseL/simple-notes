import React from 'react';
import { withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';
import { Button, Grid, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import actions from './actions';
import NotesList from '../NotesList';

const Notes = ({notes, LoadData, AddNoteLocally}) => {
  if(notes.status === 'failed') return <Redirect to="/" />
  if(notes.status === 'init') LoadData();
  let content = <div></div>;
  if(notes.status === 'success'){
    if(notes.payload.length === 0) content = <h3 className="text-center googleFont">У вас пока нет ни одной заметки.</h3>
    else content = (
      <Grid>
        <Row>
          <NotesList notes={notes.payload} />
        </Row>
      </Grid>
    )
  }

  const render = (
    <Grid>
      <div className="text-center">
        <Button onClick={AddNoteLocally} bsStyle="default">Добавить заметку</Button>
      </div>
      {content}
    </Grid>
  )

  return render;
}

const mapStateToProps = state => {
  return state.notes;
}

const mapDispatchToProps = dispatch => ({
  LoadData: actions.loadData(dispatch),
  AddNoteLocally: actions.addNoteLocally(dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Notes));
