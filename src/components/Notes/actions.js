import axios from 'axios';

const loadData = dispatch => () => {
  dispatch({type: 'NOTES_GET_START'});
  axios.get(global.urls.getNotes, {withCredentials: true})
  .then(({data: notes}) => {
    dispatch({type: 'NOTES_GET_SUCCESS', payload: notes})
  })
  .catch(res => {
    dispatch({type: 'NOTES_GET_FAILED'});
    dispatch({type: 'GET_USER_INFO_FAILED'});
  })
}

const addNoteLocally = (dispatch, note) => () => {
  dispatch({type: 'NOTE_ADD_ONE_LOCALLY'});
}

export default {
  loadData,
  addNoteLocally
}
